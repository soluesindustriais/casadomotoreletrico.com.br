<?
$h1         = 'Produtos';
$title      = 'Produtos';
$desc       = 'Encontre tudo para supermercado das melhores empresas. Receba diversos comparativos de preços pelo formulário com mais de 200 fornecedores. É grátis!';
$key        = 'produtos, materiais, equipamentos';
$var        = 'produtos';
include('inc/head.php');
?>
</head>
<body>

<? include('inc/topo.php');?>
<div class="wrapper">
 <main>
    <div class="content">
      <?=$caminho?>
      <h1>Produtos</h1>   
      <article class="full">   
        <p>Encontre diversos produtos de aço das melhores empresas, para suas necessidades. Receba diversos comparativos pelo formulário com mais de 200 fornecedores.</p>
        <ul class="thumbnails-main">

          <li>
            <a rel="nofollow" href="<?=$url?>corrente-categoria" title="Corrente"><img src="imagens/produtos/corrente-01.jpg" alt="Corrente" title="Corrente"/></a>
            <h2><a href="<?=$url?>corrente-categoria" title="Corrente">Corrente</a></h2>
          </li>

          <li>
            <a rel="nofollow" href="<?=$url?>eletrico-categoria" title="Elétrico"><img src="imagens/produtos/eletrico-01.jpg" alt="Elétrico" title="Elétrico"/></a>
            <h2><a href="<?=$url?>eletrico-categoria" title="Elétrico">Elétrico</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url?>hidraulico-categoria" title="Hidráulico"><img src="imagens/produtos/hidraulico-01.jpg" alt="Hidráulico" title="Hidráulico"/></a>
            <h2><a href="<?=$url?>hidraulico-categoria" title="Hidráulico">Hidráulico</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url?>inducao-categoria" title="Indução"><img src="imagens/produtos/inducao-01.jpg" alt="Indução" title="Indução"/></a>
            <h2><a href="<?=$url?>inducao-categoria" title="Indução">Indução</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url?>manutencao-categoria" title="Manutenção"><img src="imagens/produtos/manutencao-01.jpg" alt="Manutenção" title="Manutenção"/></a>
            <h2><a href="<?=$url?>manutencao-categoria" title="Manutenção">Manutenção</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url?>motores-categoria" title="Motores"><img src="imagens/produtos/motores-01.jpg" alt="Motores" title="Motores"/></a>
            <h2><a href="<?=$url?>motores-categoria" title="Motores">Motores</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url?>pecas-categoria" title="Peças"><img src="imagens/produtos/motores-01.jpg" alt="Peças" title="Peças"/></a>
            <h2><a href="<?=$url?>pecas-categoria" title="Peças">Peças</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url?>portao-categoria" title="Portão"><img src="imagens/produtos/portao-01.jpg" alt="Portão" title="Portão"/></a>
            <h2><a href="<?=$url?>portao-categoria" title="Portão">Portão</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url?>rebobinamento-categoria" title="Rebobinamento"><img src="imagens/produtos/rebobinamento-01.jpg" alt="Rebobinamento" title="Rebobinamento"/></a>
            <h2><a href="<?=$url?>rebobinamento-categoria" title="Rebobinamento">Rebobinamento</a></h2>
          </li>
           <li>
            <a rel="nofollow" href="<?=$url?>servicos-categoria" title="servicos"><img src="imagens/produtos/servicos-01.jpg" alt="servicos" title="servicos"/></a>
            <h2><a href="<?=$url?>servicos-categoria" title="servicos">Serviços</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url?>bombas-magneticas-categoria" title="bombas-magneticas"><img src="imagens/produtos/bombas-magneticas-01.jpg" alt="bombas-magneticas" title="bombas-magneticas"/></a>
            <h2><a href="<?=$url?>bombas-magneticas-categoria" title="Rebobinamento">Bombas Magneticas</a></h2>
          </li>
        </ul>
      </article>
    </div>
  </main>
  <? include('inc/form-mpi.php');?>
</div>
<? include('inc/footer.php');?>

</body>
</html>