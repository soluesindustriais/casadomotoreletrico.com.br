<!-- Header start-->
<header>
  <div class="top_nav">
    <div class="nav_menu">
      <nav>
        <div class="nav toggle">
          <a id="menu_toggle"><i class="fa fa-bars"></i></a>
        </div>

        <ul class="nav navbar-nav navbar-right">

          <li class="">
            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <?= Check::Image('../doutor/uploads/' . $user_cover, $user_name, ''); ?> <?= $user_name; ?> <?= $user_lastname; ?>
              <span class=" fa fa-angle-down"></span>
            </a>
            <ul class="dropdown-menu dropdown-usermenu pull-right">
              <li><a href="painel.php?exe=profile/profile">Meu Perfil</a></li>
              <li><a href="painel.php?exe=profile/update">Editar perfil</a></li>                        
              <li><a href="painel.php?exe=logout"><i class="fa fa-sign-out pull-right"></i> Sair</a></li>
            </ul>
          </li>

          <li class="">
            <a href="javascript:;" class="dropdown-toggle info-number j_retIdioma" data-idioma="<?= $_SESSION['userlogin']['user_empresa']; ?>" data-toggle="dropdown" aria-expanded="false">
              Carregando idioma... <i class="fa fa-flag"></i>           
            </a>
            <ul  class="dropdown-menu dropdown-usermenu" >
              <?php
              $ReadIdi = new Read;
              $ReadIdi->ExeRead(TB_EMP, "WHERE empresa_idioma_status != :st AND empresa_id != :id AND ( empresa_id = :lang OR empresa_parent = :lang ) ORDER BY empresa_idioma_name ASC", "st=3&id=" . EMPRESA_MASTER . "&lang={$_SESSION['userlogin']['user_empresa']}");
              if (!$ReadIdi->getResult()):
                $reademp = $_SESSION['userlogin']['user_empresa'];
              else:
                $reademp = $ReadIdi->getResult();
                $reademp = $reademp[0]['empresa_parent'];
              endif;
              $ReadIdi->ExeRead(TB_EMP, "WHERE empresa_idioma_status != :st AND empresa_id != :id AND empresa_id = :sess OR ( empresa_idioma_status != :st AND empresa_id != :id AND (empresa_id = :principal OR empresa_parent = :principal)) ORDER BY empresa_idioma_name ASC", "st=3&id=" . EMPRESA_MASTER . "&sess={$_SESSION['userlogin']['user_empresa']}&principal={$reademp}");
              if (!$ReadIdi->getResult()): ?>
              <li><a><span class="pull-right"><i class="fa fa-flag"></i></span> -- Cadastre novos idiomas --</a></li>
                <?php else:
                foreach ($ReadIdi->getResult() as $idiomas):
                  ?>
                  <li><a href="<?= $idiomas['empresa_id']; ?>" class="j_setIdioma" data-idioma="<?= $idiomas['empresa_idioma_name']; ?>"><span class="pull-right"><?= Check::GetIdioma($idiomas['empresa_idioma']); ?></span><?= $idiomas['empresa_idioma_name']; ?></a></li>
                      <?php
                    endforeach;
                  endif;
                  ?>
            </ul>
          </li>

        </ul>
      </nav>
    </div>
  </div>
</header>
<!-- Header end-->