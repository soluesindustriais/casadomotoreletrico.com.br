<?php
header('Content-Type: application/json');
define('IS_API', true);

// Inclua o arquivo de configuração
$configPath = '../_app/Config.inc.php';


if (file_exists($configPath)) {
    require_once $configPath;
} else {
    http_response_code(500); // Erro interno do servidor
    echo json_encode(['error' => 'Arquivo de configuração não encontrado!']);
    exit;
}

// Verifica o método de requisição
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    http_response_code(405); // Método não permitido
    echo json_encode(['error' => 'Método não permitido. Use POST.']);
    exit;
}

// Verifica o token no POST
$token = $_POST['token'] ?? null;
if ($token !== TOKEN_API) {
    http_response_code(401); // Não autorizado
    echo json_encode(['error' => 'Acesso negado. Token inválido.']);
    exit;
}

// Verifica se os campos obrigatórios foram enviados
$requiredFields = ['blog_title', 'cat_parent', 'user_id', 'blog_status', 'blog_content', 'user_empresa'];
$missingFields = [];

foreach ($requiredFields as $field) {
    if (!isset($_POST[$field]) || $_POST[$field] === '' || $_POST[$field] === null) {
        $missingFields[] = $field;
    }
}

if (!empty($missingFields)) {
    http_response_code(400); // Requisição inválida
    echo json_encode(['error' => 'Campos obrigatórios ausentes: ' . implode(', ', $missingFields)]);
    exit;
}

// Sanitiza e prepara os dados para inserção
$post = filter_input_array(INPUT_POST, [
    'blog_title' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
    'cat_parent' => FILTER_VALIDATE_INT,
    'user_id' => FILTER_VALIDATE_INT,
    'blog_status' => FILTER_VALIDATE_INT,
    'blog_keywords' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
    'blog_description' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
    'blog_brevedescription' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
    'blog_content' => FILTER_UNSAFE_RAW,
    'blog_schedule' => FILTER_DEFAULT,
    'user_empresa' => FILTER_VALIDATE_INT
]);

// Remova 'gallery_file' do array de dados, já que ele não deve ser enviado ao banco
unset($post['gallery_file']);

// Verifica se a validação falhou
if (false === $post['cat_parent']) {
    http_response_code(400);
    echo json_encode(['error' => 'ID da categoria deve ser um número inteiro.']);
    exit;
}

if (false === $post['user_id']) {
    http_response_code(400);
    echo json_encode(['error' => 'ID do usuário deve ser um número inteiro.']);
    exit;
}

if (null === $post['blog_status']) {
    http_response_code(400);
    echo json_encode(['error' => 'O status do blog deve ser verdadeiro (true) ou falso (false).']);
    exit;
}

// Verifica e processa o arquivo 'blog_cover'
$file = [];
if (isset($_FILES['blog_cover']) && $_FILES['blog_cover']['error'] === UPLOAD_ERR_OK) {
    $file['blog_cover'] = $_FILES['blog_cover'];
} else {
    http_response_code(400);
    echo json_encode(['error' => 'Arquivo de capa não enviado ou erro de envio.']);
    exit;
}

// Combine os dados de postagem e arquivos
$posts = array_merge($post, $file);

try {
    $blog = new Blog();
    $blog->ExeCreate($posts);
    
    if ($result = $blog->getResult()) {
        http_response_code(201); // Criado
        echo json_encode($result);

    } else {
        http_response_code(500); // Erro interno do servidor
        echo json_encode(['error' => $blog->getError()]);
    }
} catch (Exception $e) {
    http_response_code(500); // Erro interno do servidor
    echo json_encode(['error' => 'Erro ao criar postagem: ' . $e->getMessage()]);
}
