<?php
$s = urldecode($URL[1]);
$h1 = 'Pesquisa no site';
$title = 'Pesquisa';
$desc = 'Pesquisa no site por produtos comercializados. Veja esses e outros produtos comercializados. Dúvidas? Entre em contato conosco e fique por dentro de novidades.';
$key = '';
$var = 'Pesquisa de produtos';
include('inc/head.php');
?>
</head>
<body>
  <?php include('inc/topo-blog.php'); ?>
  <main>
    <div id="pesquisa">
      <section>
        <?= $caminho ?>
        <div class="container">
          <div class="wrapper">
            <h2>Você pesquisou por: <br> <span class="xx-large text-uppercase"><?= $s; ?></span></h2>
            <?php
            include('inc/paginacao-inc.php');
            $Read->ExeRead(TB_PRODUTO, "WHERE prod_status = :st AND user_empresa = :emp AND (prod_title LIKE '%' :s '%' OR prod_description LIKE '%' :s '%' OR prod_content LIKE '%' :s '%' OR prod_keywords LIKE '%' :s '%' OR prod_brevedescription LIKE '%' :s '%') LIMIT :limit OFFSET :offset", "st=2&emp=" . EMPRESA_CLIENTE . "&s={$s}&limit={$Pager->getLimit()}&offset={$Pager->getOffset()}");
            if (!$Read->getResult()):  
            $Read->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_status = :stats AND (cat_title LIKE '%' :s '%' OR cat_description LIKE '%' :s '%' OR cat_content LIKE '%' :s '%') LIMIT :limit OFFSET :offset", "emp=" . EMPRESA_CLIENTE . "&stats=2&s={$s}&limit={$Pager->getLimit()}&offset={$Pager->getOffset()}");
              if (!$Read->getResult()):
                WSErro("Desculpe, mas sua pesquisa para <b>{$s}</b> não retornou resultados. Talvez você queira utilizar outros termos! Você ainda pode usar nosso menu de navegação para encontrar o que procura!", WS_INFOR, null, "Resultado da pesquisa");
              else: ?>
                <div class="container">
                  <div class="grid grid-col-4">
                    <?php
                    foreach ($Read->getResult() as $cat):
                      extract($cat); ?>
                      <div class="search-card">
                        <a href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $cat_name; ?>" title="<?= $cat_title; ?>">
                          <?php
                          if (empty($cat_cover)):
                            $Read->ExeRead(TB_PRODUTO, "WHERE user_empresa = :emp AND prod_status = :stats AND FIND_IN_SET(:cat, cat_parent) ORDER BY prod_date DESC LIMIT 1", "emp=" . EMPRESA_CLIENTE . "&stats=2&cat={$cat_id}");
                            if ($Read->getResult()):
                              foreach ($Read->getResult() as $prod): ?>
                                <img class="search-card__cover" src="<?=RAIZ?>/doutor/uploads/<?=$prod_cover?>" alt="<?=$cat_title?>" title="<?=$cat_title?>">
                                <?
                              endforeach;
                            else: ?>
                              <img class="search-card__cover" src="<?=RAIZ?>/doutor/images/default.png" alt="<?=$cat_title?>" title="<?=$cat_title?>">
                             <?
                            endif;
                          else:?>
                            <img class="search-card__cover" src="<?=RAIZ?>/doutor/uploads/<?=$cat_cover?>" alt="<?=$cat_title?>" title="<?=$cat_title?>">
                            <?
                          endif; ?>
                          <h2 class="search-card__title"><?= $cat_title; ?></h2>
                        </a>
                      </div>
                    <?php endforeach; ?>
                  </div>
                </div>
                <div class="float-right">
                  <?php
                  $Pager->ExePaginator(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_status = :stats AND (cat_title LIKE '%' :s '%' OR cat_description LIKE '%' :s '%' OR cat_content LIKE '%' :s '%')", "emp=" . EMPRESA_CLIENTE . "&stats=2&s={$s}");
                  echo $Pager->getPaginator(); ?>
                </div>
              <? endif;
            else: ?>
              <div class="container">
                <div class="grid grid-col-4">
                  <?php
                  foreach ($Read->getResult() as $prod):
                    extract($prod); ?>
                    <div class="search-card">
                      <a href="<?= RAIZ . '/' . trim(Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $prod_name, '/'); ?>" title="<?= $prod_title; ?>">
                        <img class="search-card__cover" src="<?=RAIZ?>/doutor/uploads/<?=$prod_cover?>" alt="<?=$prod_title?>" title="<?=$prod_title?>">
                        <h2 class="search-card__title"><?= $prod_title; ?></h2>
                      </a>
                    </div>
                  <? endforeach; ?>
                </div>
              </div>
              <div class="float-right">
                <?php
                $Pager->ExePaginator(TB_PRODUTO, "WHERE prod_status = :st AND user_empresa = :emp AND (prod_title LIKE '%' :s '%' OR prod_description LIKE '%' :s '%' OR prod_content LIKE '%' :s '%')", "st=2&emp=" . EMPRESA_CLIENTE . "&s={$s}");
                echo $Pager->getPaginator(); ?>
              </div>
            <? endif; ?>
          </div> <!-- wrapper -->
          <div class="clear"></div>
        </div> <!-- container -->
      </section>
    </div> <!-- pesquisa -->
  </main>
  <?php include('inc/footer-blog.php'); ?>
</body>
</html>