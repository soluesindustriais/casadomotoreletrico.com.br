
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<script src="js/jquery.hoverdir.js"></script> <script> $(function() { $(' .thumbnails-mod17 > li ').each( function() { $(this).hoverdir({ hoverDelay : 75}); } ); });</script><?php $lista = array('Escova de carvão para motor elétrico','Motor de portão automático','Motor de portão basculante','Motor de portão deslizante','Motor de portao eletrico','Motor para porta de aço','Motor para porta de enrolar','Motor para porta de enrolar automática','Motor para portão','Motor para portão automático','Motor para portão automático preço','Motor para portão basculante','Motor para portão basculante ppa','Motor para portão basculante preço','Motor para portão de correr','Motor para portão de elevação','Motor para portão de garagem','Motor para portão deslizante','Motor para portão deslizante ppa','Motor para portão elétrico','Motor para portão eletronico','Motor para portão eletronico basculante','Motor para portão eletrônico deslizante','Motor para portão eletronico ppa','Motor para portão ppa','Motor portao automatico','Motor portão basculante','Motor portao correr','Motor portão de correr','Motor portão deslizante','Motor portao eletronico','Motor portao eletronico basculante','Motor portão eletrônico basculante preço','Motores eletricos para portão','Preço de motor para portão','Preço de motor para portão eletronico','Preço motor portao eletronico'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "portao";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/portao/portao-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-22"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/portao/portao-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/portao/thumbs/portao-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>