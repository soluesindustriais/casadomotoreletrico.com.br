<div class="grid"><div class="col-6"><div class="picture-legend picture-center"><a class="lightbox" href="<?=$url?>imagens/servicos/servicos-01.jpg" title="<?=$h1?>" target="_blank"><img class="lazyload" data-src="<?=$url?>imagens/servicos/thumbs/servicos-01.jpg" alt="<?=$h1?>" title="<?=$h1?>" /></a><strong>Imagem ilustrativa de <?=$h1?></strong></div> </div><div class="col-6"><div class="picture-legend picture-center"><a class="lightbox" href="<?=$url?>imagens/servicos/servicos-02.jpg" title="<?=$h1?>" target="_blank"><img class="lazyload" data-src="<?=$url?>imagens/servicos/thumbs/servicos-02.jpg" alt="<?=$h1?>" title="<?=$h1?>" /></a><strong>Imagem ilustrativa de <?=$h1?></strong></div> </div></div>
                    <script type="application/ld+json">
                    {
                        "@context": "https://schema.org",
                        "@type": "ItemList",
                        "itemListElement": [{
                                "@type": "ImageObject",
                                "author": "Soluções Industriais",
                                "contentUrl": "<?= $url ?>imagens/servicos/thumbs/servicos-01.jpg",
                                "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto.",
                                "name": "<?= $h1 ?> modelo 01",
                                "uploadDate": "2024-02-22"
                            },
                            {
                                "@type": "ImageObject",
                                "author": "Soluções Industriais",
                                "contentUrl": "<?= $url ?>imagens/servicos/thumbs/servicos-02.jpg",
                                "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto.",
                                "name": "<?= $h1 ?> modelo 02",
                                "uploadDate": "2024-02-22"
                            }
                        ]
                    }
                    </script>
                    