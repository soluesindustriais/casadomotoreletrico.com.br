
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<script src="js/jquery.hoverdir.js"></script> <script> $(function() { $(' .thumbnails-mod17 > li ').each( function() { $(this).hoverdir({ hoverDelay : 75}); } ); });</script><?php $lista = array('Acionamento de motores','Acionamento de motores com inversor de frequência','Acionamento de motores elétricos','Acionamento de motores elétricos preço','Acionamento de motores preço','Acionamento de motores sp','Alugar de motor vibrador a gasolina','Aluguel de motor estacionário','Aluguel de motor vibrador a gasolina','Análise de circuito elétrico de motores','Análise de vibração de motores','Análise de vibração de motores elétricos','Análise de vibração de motores elétricos preço','Análise de vibração em motores','Análise de vibração em motores elétricos','Análise de vibração em motores preço','Análise termográfica em motores elétricos','Locação de motor vibrador a gasolina','Locação de motor vibrador carrapato','Prçeo acionamento de motores elétricos','Preço acionamento de motores','Preço termografia em motores elétricos','Serviço em motobomba','Serviço em motor elétrico','Serviço em motor elétrico quanto custa','Serviço em motor elétrico sp','Termografia em motores elétricos','Termografia em motores elétricos preço','Termografia em motores elétricos sp','Termografia industrial em motores elétricos','Transformadores elétricos para motor'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "servicos";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/servicos/servicos-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-22"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/servicos/servicos-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/servicos/thumbs/servicos-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>