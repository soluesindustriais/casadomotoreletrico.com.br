
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<script src="js/jquery.hoverdir.js"></script> <script> $(function() { $(' .thumbnails-mod17 > li ').each( function() { $(this).hoverdir({ hoverDelay : 75}); } ); });</script><?php $lista = array('Comprar motor hidraulico','Conserto de cilindro de motor hidráulico','Conserto de motor hidráulico de pistão','Fabricante de motores hidráulicos','Manutenção de cilindros de motores hidráulicos','Manutenção de motores hidráulicos','Manutenção motor hidráulico','Motor hidraulico','Motor hidraulico de engrenagem','Motor hidraulico de paletas','Motor hidraulico de pistão','Motor hidraulico eaton','Motor hidraulico orbital','Motor hidraulico parker','Motor hidraulico pequeno','Motor hidraulico preço','Motor hidraulico radial','Motor hidráulico rotativo','Motor hidráulico rotor','Motor hidraulico sauer danfoss','Motor hidraulico usado','Motor hidraulico usado a venda','Motores hidráulicos poclain','Peças para motor hidráulico','Recondicionamento de motor hidráulico','Reforma de cilindros de motores hidráulicos','Reforma de motores hidráulicos','Reforma de motores hidráulicos de pistão','Reforma de motores hidráulicos vickers','Remanufatura de motor hidráulico','Reparo de motores hidráulicos'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "bombas-magneticas";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/bombas-magneticas/bombas-magneticas-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-22"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/bombas-magneticas/bombas-magneticas-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/bombas-magneticas/thumbs/bombas-magneticas-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>