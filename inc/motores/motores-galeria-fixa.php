
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>

<script src="js/jquery.hoverdir.js"></script>
<script>
    $(function() {
        $(' .thumbnails-mod17 > li ').each(function() {
            $(this).hoverdir({
                hoverDelay: 75
            });
        });
    });
</script>
<?php $lista = array('Autotransformador de partida de motores', 'Bomba peristáltica com motor elétrico', 'Bomba peristáltica com motor pneumático', 'Centro de controle de motores', 'Centro de controle de motores de baixa tensão', 'Cnc spindle motor', 'Comprar motor spindle', 'Conversor ca cc para motor cc', 'Distribuidor de motor spindle', 'Micro motor elco', 'Micro motor elgin', 'Motobomba a gasolina', 'Motobomba centrífuga', 'Motobomba de água', 'Motobomba diesel', 'Motor cc', 'Motor com redutor', 'Motor de indução', 'Motor de partida para tratores', 'Motor hercules', 'Motor intercambiável', 'Motor para camara fria', 'Motor para empilhadeira', 'Motor para motobomba', 'Motor pneumatico', 'Motor pneumático mtrr', 'Motor pneumático mtrr 80 eld', 'Motor síncrono torque', 'Motor spindle mac65', 'Motor spindle mwc 80 1 5kw', 'Motor spindle mwc 80 2 2kw', 'Motor spindle preço', 'Motor vibrador', 'Motor vibratório', 'Motoredutor', 'Motoredutor com rotor cônico', 'Motores industriais', 'Motores para queimadores', 'Motorredutor para tanques de resfriamento de leite', 'Motorredutor pendular', 'Motorredutor planetário', 'Recondicionamento de motor parker', 'Revendedor de motor spindle', 'Spindle motor');
                                shuffle($lista);
                                for ($i = 1; $i < 13; $i++) { ?>  
<?php
$folder = "motores";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/motores/motores-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-22"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/motores/motores-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/motores/thumbs/motores-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>