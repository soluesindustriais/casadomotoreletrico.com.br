
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<script src="js/jquery.hoverdir.js"></script> <script> $(function() { $(' .thumbnails-mod17 > li ').each( function() { $(this).hoverdir({ hoverDelay : 75}); } ); });</script><?php $lista = array('Acoplamento flexível para motor','Acoplamento flexível para motor de passo','Acoplamento para motor','Acoplamento para motor elétrico','Bico injetor motor maxion 2.5','Bico injetor motor mwm sprint','Cabo de motor','Chave de segurança para motores','Coletor de dados motorola','Coletor motorola mc3190','Compactador de placa com motor branco','Compactador de placa com motor honda','Compactador de placa reversível com motor à diesel','Compactador de placa reversível com motor honda à gasolina','Compactador de placa reversível sem motor','Coxim de borracha para motor','Cremalheira do motor','Desengraxante para motor','Disjuntor motor','Disjuntor para motor','Equipamentos para motor aeronáutico','Escova carvao motor eletrico','Escova de motor de arranque','Escova motor de arranque','Escovas de carvao para motores','Escovas de grafite para motores','Escovas para motores','Escovas para motores e geradores elétricos','Excitatriz para motor sincrono','Fabricante de retentores para motor de popa','Flexível para escape de motores','Medidor de compressão do motor','Medidores de compressão para motores','Megôhmetro para motores','Neodímio motor magnético','Neodimio para motor','Painel para motor estacionário','Patins isoladores para motores','Peças de motores de avião','Peças para motor aeronáutico','Peças para motor de trator','Peças para motor de tratores volvo','Porta escova motor cc','Porta escova motor de partida','Porta escovas para motores','Promotor de aderencia','Redutor para motor','Retentores para motor de popa','Selante neutro para motores','Selante para motor'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "pecas";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/pecas/pecas-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-22"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/pecas/pecas-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/pecas/thumbs/pecas-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>