
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<script src="js/jquery.hoverdir.js"></script> <script> $(function() { $(' .thumbnails-mod17 > li ').each( function() { $(this).hoverdir({ hoverDelay : 75}); } ); });</script><?php $lista = array('Assistência técnica em motores de passo','Conserto de moto bombas em sp','Conserto de moto bombas no abc','Conserto de motobomba','Conserto de motor de indução','Conserto de motor de piscina','Conserto de motor de pistão','Conserto de motor de portao automatico','Conserto de motores eletricos','Conserto de motores elétricos em jundiaí','Conserto de motores elétricos em sorocaba','Conserto de motorredutor','Conserto em motor de passo','Conserto motor eletrico','Conserto motor piscina','Conserto motores elétricos campinas','Empresas de manutenção de motores elétricos em sorocaba','Manutenção de motor dc alto torque','Manutenção de motor dc driver','Manutenção de motor de passo','Manutenção de motor elétrico campinas','Manutenção de motor elétrico cc','Manutenção de motor elétrico monofásico','Manutenção de motor rexroth','Manutenção de motoredutores','Manutenção de motores aeronáuticos','Manutenção de motores cc','Manutenção de motores cc em jundiai','Manutenção de motores dc','Manutenção de motores elétricos','Manutenção de motores elétricos em campinas','Manutenção de motores elétricos em campinas preço','Manutenção de motores elétricos em jundiaí','Manutenção de motores elétricos em sorocaba','Manutenção de motores especiais','Manutenção de rotores de motor','Manutenção dos motores elétricos em sorocaba','Manutenção e montagem de motores elétricos','Manutenção em moto redutor','Manutenção em motores elétricos','Manutenção motobomba','Manutenção preditiva em motores elétricos','Manutenção preventiva de motores elétricos','Manutenção preventiva em motores elétricos','Manutenção preventiva motores cc','Manutenção preventiva servo motor','Oficina de motores eletricos','Remanufatura de motor bosch','Reparo de motor monofásico','Reparo de motores elétricos','Reparo de motores industriais','Serviços de manutenção em motores cc ca'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "manutencao";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/manutencao/manutencao-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-22"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/manutencao/manutencao-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/manutencao/thumbs/manutencao-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>