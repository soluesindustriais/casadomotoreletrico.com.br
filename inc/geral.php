<?
$nomeSite			= 'Casa do Motor Elétrico';
$slogan				= 'Cotações de diversas empresa de Motores Elétricos';
//$url				= 'https://www.solucoesindustriais.com.br/lista/site-base/';
//$url				= 'https://www.casadomotoreletrico.com.br/';
//$url				= 'https://localhost/casadomotoreletrico.com.br/';

$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") { $url = $http."://".$host."/"; }
else { $url = $http."://".$host.$dir["dirname"]."/";  }


$emailContato		= 'everton.lima@solucoesindustriais.com.br';
$rua				= 'Rua Pequetita, 179';
$bairro				= 'Vila Olimpia';
$cidade				= 'São Paulo';
$UF					= 'SP';
$cep				= 'CEP: 04552-060';
$latitude			= '-22.546052';
$longitude			= '-48.635514';
$idAnalytics		= 'UA-130632441-1';
$senhaEmailEnvia	= '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode			= explode("/", $_SERVER['PHP_SELF']);
$urlPagina 			= end($explode);
$urlPagina	 		= str_replace('.php','',$urlPagina);
$urlPagina 			== "index"? $urlPagina= "" : "";
$urlAnalytics = str_replace("https://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
$tagsanalytic = ["G-CYR35C2ZSQ","G-D23WW3S4NC"];
//reCaptcha do Google
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';


$isMobile =  preg_match("/(android|webos|avantgo|iphone|ipad|ipod|blackberry|iemobile|bolt|boost|cricket|docomo|fone|hiptop|mini|opera mini|kitkat|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]); 
//Breadcrumbs
$caminho ='
	<div class="breadcrumb" id="breadcrumb  ">
		<div class="wrapper">
			<div class="bread__row">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
		<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <a href="'.$url.'" itemprop="item" title="Home">
			<span itemprop="name"> Home  </span>
		  </a>
		  <meta itemprop="position" content="1" />
		</li>   
		<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <span itemprop="name">'.$h1.'</span>
		  <meta itemprop="position" content="2" />
		</li>
	  </ol>
	</nav>
	</div>
	</div>
	</div>
	  ';

$caminho2 ='
	<div class="breadcrumb" id="breadcrumb  ">
		<div class="wrapper">
			<div class="bread__row">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
		<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <a href="'.$url.'" itemprop="item" title="Home">
			<span itemprop="name"> Home  </span>
		  </a>
		  <meta itemprop="position" content="1" />
		</li>   
		<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
		<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <span itemprop="name">'.$h1.'</span>
		  <meta itemprop="position" content="3" />
		</li>
	  </ol>
	</nav>
	</div>
	</div>
	</div>
	';

$caminhobombas_magneticas='
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'bombas-magneticas-categoria" itemprop="item" title="Bombas-magneticas - Categoria">
                      <span itemprop="name">Bombas magneticas - Categoria  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhocorrente='
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'corrente-categoria" itemprop="item" title="Corrente - Categoria">
                      <span itemprop="name">Corrente - Categoria  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhoeletrico='
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'eletrico-categoria" itemprop="item" title="Eletrico - Categoria">
                      <span itemprop="name">Eletrico - Categoria  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhohidraulico='
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'hidraulico-categoria" itemprop="item" title="Hidraulico - Categoria">
                      <span itemprop="name">Hidraulico - Categoria  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhoinducao='
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'inducao-categoria" itemprop="item" title="Inducao - Categoria">
                      <span itemprop="name">Inducao - Categoria  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhomanutencao='
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'manutencao-categoria" itemprop="item" title="Manutencao - Categoria">
                      <span itemprop="name">Manutencao - Categoria  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhomotores='
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'motores-categoria" itemprop="item" title="Motores - Categoria">
                      <span itemprop="name">Motores - Categoria  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhopecas='
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'pecas-categoria" itemprop="item" title="Pecas - Categoria">
                      <span itemprop="name">Pecas - Categoria  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhoportao='
               <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'portao-categoria" itemprop="item" title="Portao - Categoria">
                      <span itemprop="name">Portao - Categoria  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhorebobinamento='
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'rebobinamento-categoria" itemprop="item" title="Rebobinamento - Categoria">
                      <span itemprop="name">Rebobinamento - Categoria  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhoservicos='
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'servicos-categoria" itemprop="item" title="Servicos - Categoria">
                      <span itemprop="name">Servicos - Categoria  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

