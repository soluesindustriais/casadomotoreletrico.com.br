
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<script src="js/jquery.hoverdir.js"></script> <script> $(function() { $(' .thumbnails-mod17 > li ').each( function() { $(this).hoverdir({ hoverDelay : 75}); } ); });</script><?php $lista = array('Escova para motor corrente continua','Escovas para motor corrente alternada','Manutenção de motor corrente alternada','Manutenção de motores corrente contínua','Manutenção em motores elétricos de corrente alternada','Motor corrente alternada','Motor corrente alternada preço','Motor corrente alternada sp','Motor corrente continua 24v','Motor corrente continua aplicações','Motor corrente continua preço','Motor corrente continua weg','Motor de corrente alternada','Motor de corrente alternada siemens','Motor de corrente continua','Motor de corrente continua 12v','Motor de corrente contínua preço','Motor elétrico corrente alternada','Motor elétrico corrente contínua','Motor elétrico corrente contínua sp','Motor eletrico de corrente continua','Motor para irrigação elétrico','Motores 12v corrente continua','Motores corrente alternada ca','Motores corrente contínua cc','Motores de corrente continua aplicações','Motores de corrente contínua torque','Motores elétricos de corrente contínua weg','Preço motor corrente'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "corrente";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/corrente/corrente-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-22"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/corrente/corrente-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/corrente/thumbs/corrente-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>