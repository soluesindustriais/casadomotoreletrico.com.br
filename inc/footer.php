<footer>
    <div class="wrapper">
        <div class="contact-footer">
            <address>
                <span><?=$nomeSite." - ".$slogan?></span>
            </address>
        </div>
        <div class="menu-footer">
            <nav>
                <ul>
                    <li><a href="<?=$url?>" title="PÃ¡gina inicial">Home</a></li>
                    <li><a href="<?=$url?>sobre-nos" title="Sobre nÃ³s">Sobre nÃ³s</a></li>
                    <li><a href="<?=$url?>produtos-categoria" title="Produtos">Produtos</a></li>
                    <li><a href="<?=$url?>mapa-site" title="Mapa do site">Mapa do site</a></li>
                    <li><a href="https://faca-parte.solucoesindustriais.com.br" target="_blank" title="FaÃ§a parte">FaÃ§a parte</a></li>
                </ul>
            </nav>
        </div>
        <br class="clear">
    </div>
</footer>
<div class="copyright-footer">
    <div class="wrapper-footer">
        <div class="copy">Copyright Â© <?=$nomeSite?>. (Lei 9610 de 19/02/1998)</div>
        <div class="center-footer">
            <img class="lazyload" data-src="imagens/img-home/logo-footer.png" alt="<?=$nomeSite?>"
                title="<?=$nomeSite?>" style="width: 100px; height: 100px; object-fit: contain;">
            <p>Ã© um parceiro</p>
            <img class="lazyload" data-src="imagens/logo-solucs.png" alt="SoluÃ§Ãµes Industriais"
                title="SoluÃ§Ãµes Industriais" style="width: 100px; height: 100px; object-fit: contain;">
        </div>
        <div class="selos">
            <a rel="noopener nofollow" href="http://validator.w3.org/check?uri=<?=$url.$urlPagina?>" target="_blank"
                title="HTML5 W3C"><i class="fab fa-html5"></i> <strong>W3C</strong></a>
            <a rel="noopener nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?=$url.$urlPagina?>"
                target="_blank" title="CSS W3C"><i class="fab fa-css3"></i> <strong>W3C</strong></a>
        </div>
    </div>
</div>
<!-- ConteÃºdo do footer acima -->

<!-- Includes Detalhes -->
<script defer src="<?=$url?>js/geral.js"></script>
<script async src="<?=$url?>js/app.js"></script>
<script src="js/click-actions.js"></script>
<script>
    function toggleDetails() {
        var detailsElement = document.querySelector(".webktbox");
        
        // Verificar se os detalhes estÃ£o abertos ou fechados
        if (detailsElement.hasAttribute("open")) {
            // Se estiver aberto, rolar suavemente para cima
            window.scrollTo({
                top: 200,
                behavior: "smooth"
            });
        } else {
            // Se estiver fechado, rolar suavemente para baixo (apenas 100px)
            window.scrollTo({
                top: 1300,
                behavior: "smooth"
            });
        }
    }
</script>
<? include "inc/readmore.php"?>
<!-- API botao-cotar -->
<script src="https://solucoesindustriais.com.br/js/dist/sdk-cotacao-solucs/package.js"></script>
<script>
    var guardar = document.querySelectorAll('.botao-cotar');
    for (var i = 0; i < guardar.length; i++) {
        guardar[i].removeAttribute('href');
        var adicionando = guardar[i].parentNode;
        adicionando.classList.add('nova-api');
    };
</script>
<!-- Script Aside-Launcher start -->

<script src="https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js" defer></script>
<script>
    const aside = document.querySelector('aside');
    const data = '<div data-sdk-ideallaunch data-segment="SoluÃ§Ãµes Industriais - Oficial"></div>';
aside != null ? aside.insertAdjacentHTML('afterbegin', data) : console.log("NÃ£o hÃ¡ aside presente para o Launch");
</script>
<!-- Chat -->

<script src="//code.jivosite.com/widget/01FoIz0x6v" defer></script>
<!-- Launcher de exit -->
<!-- Outros Scripts  -->
<!-- TagManager  -->
<?php
foreach($tagsanalytic as $analytics){
    echo '<script async src="https://www.googletagmanager.com/gtag/js?id='.$analytics.'"></script>'.'<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);}'."gtag('js', new Date()); gtag('config', '$analytics')</script>";
}
?>


<!-- Script Launch end -->
<!-- <div style="display: none" id="exit-banner-div">
    <div data-sdk-ideallaunch="" data-placement="popup_exit" id="exit-banner-container"></div>
</div>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/dist/js.cookie.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />

<script src='https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js'></script>

<script>
    setTimeout(function() {
        $("html").mouseleave(function() {
            if ($("#exit-banner-container").find('img').length > 0 &&
            !Cookies.get('banner_displayed')) {
                if ($('.fancybox-container').length == 0) {
                    $.fancybox.open({
                    src: '#exit-banner-div',
                    type: 'inline',
                    opts: {
                        afterShow: function() {
                            let minutesBanner = new Date(new Date().getTime() + 5 * 60 *
                            1000);
                            Cookies.set('banner_displayed', true, {
                                expires: minutesBanner
                            });
                        }
                    }
                });
            }
        }
    });
}, 4000);
</script> -->


    


<div style="display: none" id="exit-banner-div">
    <div data-sdk-ideallaunch="" data-placement="popup_exit" id="exit-banner-container"></div>
</div>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/dist/js.cookie.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />

<script src='https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js'></script>

<script>
    setTimeout(function() {
        $("html").mouseleave(function() {
            if ($("#exit-banner-container").find('img').length > 0 &&
                !Cookies.get('banner_displayed')) {
                if ($('.fancybox-container').length == 0) {
                    $.fancybox.open({
                        src: '#exit-banner-div',
                        type: 'inline',
                        opts: {
                            afterShow: function() {
                                let minutesBanner = new Date(new Date().getTime() + 5 * 60 * 1000);
                                Cookies.set('banner_displayed', true, { expires: minutesBanner });
                            }
                        }
                    });
                }
            }
        });
    }, 4000);
</script>
        <div style="display: none" id="exit-banner-div">
    <div data-sdk-ideallaunch="" data-placement="popup_exit" id="exit-banner-container"></div>
</div>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/dist/js.cookie.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />

<script src='https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js'></script>

<script>
    setTimeout(function() {
        $("html").mouseleave(function() {
            if ($("#exit-banner-container").find('img').length > 0 &&
                !Cookies.get('banner_displayed')) {
                if ($('.fancybox-container').length == 0) {
                    $.fancybox.open({
                        src: '#exit-banner-div',
                        type: 'inline',
                        opts: {
                            afterShow: function() {
                                let minutesBanner = new Date(new Date().getTime() + 5 * 60 * 1000);
                                Cookies.set('banner_displayed', true, { expires: minutesBanner });
                            }
                        }
                    });
                }
            }
        });
    }, 4000);
</script>