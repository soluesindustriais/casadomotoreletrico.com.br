
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<script src="js/jquery.hoverdir.js"></script> <script> $(function() { $(' .thumbnails-mod17 > li ').each( function() { $(this).hoverdir({ hoverDelay : 75}); } ); });</script><?php $lista = array('Acionamento de motores elétricos trifásicos','Anéis coletores motores elétricos','Bobinagem de motores elétricos','Buchas de bronze para motores eletricos','Comprar motor eletrico','Escova para motor elétrico','Mini motor eletrico','Motor eletrico','Motor eletrico 1 cv','Motor eletrico 12v','Motor eletrico 2cv','Motor eletrico 3cv','Motor eletrico 5cv','Motor eletrico ca','Motor eletrico cc','Motor elétrico cc 4000 rpm','Motor elétrico cc aberto','Motor elétrico de indução','Motor eletrico industrial','Motor eletrico monofasico','Motor eletrico monofasico 2cv','Motor eletrico novo','Motor eletrico para carros','Motor eletrico pequeno','Motor eletrico preço','Motor eletrico trifasico','Motor eletrico usado','Motor eletrico weg','Motor pequeno eletrico','Motores elétricos onde comprar','Motores elétricos para empilhadeiras','Motores eletricos usados a venda','Porta escova para motor elétrico','Preço motor eletrico','Redutor para motor elétrico preço','Venda de motores eletricos'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "eletrico";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/eletrico/eletrico-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-22"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/eletrico/eletrico-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/eletrico/thumbs/eletrico-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>