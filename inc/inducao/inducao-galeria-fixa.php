
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<script src="js/jquery.hoverdir.js"></script> <script> $(function() { $(' .thumbnails-mod17 > li ').each( function() { $(this).hoverdir({ hoverDelay : 75}); } ); });</script><?php $lista = array('Betoneira com motor monofásico','Conserto de motor trifásico','Conserto de motor trifásico com reversão','Inversor de frequencia para motor monofasico','Inversor de frequência para motor trifásico preço','Manutenção de motores trifásicos','Motor 220v monofasico','Motor 2cv monofásico','Motor 3 cv monofasico','Motor 3cv monofasico alta rotação','Motor 3cv monofasico baixa rotação','Motor 7 5 cv trifasico preço','Motor baixa rotação monofásico','Motor de indução monofásico','Motor de indução trifásico','Motor de indução trifásico preço','Motor elétrico 2cv monofásico','Motor eletrico 4 cv monofasico','Motor elétrico de indução trifásico','Motor eletrico monofasico 1 cv','Motor elétrico monofásico 2cv 4 polos','Motor eletrico monofasico 5 cv','Motor elétrico trifásico preço','Motor indução monofásico','Motor indução trifásico','Motor monofasico','Motor monofásico 2cv','Motor monofasico 2cv alta rotação','Motor monofasico 2cv baixa rotação','Motor monofasico 2cv usado','Motor monofásico 3cv','Motor monofasico 3cv alta rotação','Motor monofasico 3cv usado','Motor monofásico 5 cv','Motor monofasico preço','Motor monofasico usado','Motor trifasico','Motor trifasico 220v','Motor trifasico 3cv preço','Motor trifásico de indução','Motor trifasico preço','Motor trifasico weg','Motorredutor trifásico','Reparo de motor trifásico'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "inducao";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/inducao/inducao-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-22"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/inducao/inducao-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/inducao/thumbs/inducao-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>