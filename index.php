<?
$h1         = 'Casa do Motor Elétrico';
$title      = 'Início';
$desc       = 'Casa do Motor Elétrico - Conte com os melhores fornecedores de equipamentos de supermercados do Brasil em um só lugar gratuitamente. Veja tudo sobre';
$var        = 'Home';
include('inc/head.php');

?>
</head>
<body>
<? include('inc/topo.php'); ?>
<section class="cd-hero">
  <div class="title-main"> <h1>MOTORES ELÉTRICOS</h1> </div>
  <ul class="cd-hero-slider autoplay">
    <li class="selected">
      <div class="cd-full-width">
        <h2>Motor Elétrico Trifásico</h2>
        <p>Buscou por Motor Elétrico Trifásico, você vai encontrar na plataforma Motores Elétricos, cote produtos agora mesmo com mais de 30 fornecedores ao mesmo tempo. É grátis!</p>
        <a href="<?=$url?>motor-eletrico-trifasico" class="cd-btn btn-intro-ml-3">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>Motor para portão</h2>
        <p>Se pesquisa por Motor para portão, encontre as melhores indústrias, faça uma cotação hoje mesmo com mais de 200 fornecedores em um só lugar gratuitamente!</p>
        <a href="<?=$url?>motor-para-portao" class="cd-btn btn-intro-ml-3">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>Motor eletrico para carros</h2>
        <p>Compare preços de Motor eletrico para carros, encontre os melhores fabricantes, realize um orçamento já com aproximadamente 100 distribuidores ao mesmo tempo gratuitamente!</p>
        <a href="<?=$url?>motor-eletrico-para-carros" class="cd-btn btn-intro-ml-3">Saiba mais</a>
      </div>
    </li>
  </ul>
  <div class="cd-slider-nav">
    <nav>
      <span class="cd-marker item-1"></span>
      <ul>
        <li class="selected"><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
      </ul>
    </nav>
  </div>
</section>
<main>
  <section class="wrapper-main">
    <div class="destaque txtcenter">
        <h2>Produtos <b>em destaque</b></h2>
      </div>
    <div class="products">
        <?php include'inc/produtos.php'; ?>
    </div>
    
    <div class="destaque txtcenter">
        <h2>Faça diversas <b>cotações</b></h2>
      </div>
      <div class="main-center">
      <div class=" quadro-2 ">
        <h2>Motores elétricos</h2>
        <div class="div-img">
          <p>Motores elétricos são equipamentos econômicos, de baixo custo, fácil manuseio, feitos com materiais resistentes e leves, com uma variedade de graus de proteção, fácil limpeza e podem ser aplicados em diversas máquinas industriais, de grande ou pequeno porte. </p>
        </div>
        <div class="gerador-svg">
          <img loading="lazy" src="imagens/img-home/motor-eletrico.png" alt="Motores Elétricos" title="Motores Elétricos">
        </div>
      </div>
      <div class=" incomplete-box">
        <ul data-anime="in">
          <li>
            <p>Com excelente potência e desempenho eficaz, entregam resultados satisfatórios e correspondem às expectativas dos clientes. São equipamentos que possuem as seguintes características:</p>
            
            <li><i class="fas fa-angle-right"></i> Não poluem</li>
            <li><i class="fas fa-angle-right"></i> Possuem grande vida útil</li>
            <li><i class="fas fa-angle-right"></i> Alta qualidade</li>
            <li><i class="fas fa-angle-right"></i> E tem manutenção acessível</li>
          </ul>
          <a href="<?=$url?>motor-eletrico" class="btn-4">Saiba mais</a>
        </div>
      </div>
      <br>
    </section>
    <section class="wrapper-img">
      <div class="txtcenter">
        <h2>Produtos <b>Relacionados</b></h2>
      </div>
      <div class="content-icons">
        <div class="produtos-relacionados-1">
          <figure>
            <a href="<?=$url?>motor-portao-eletronico">
              <div class="fig-img">
                <h2>Motor portao eletronico</h2>
                <div class="btn-5" data-anime="up"> Saiba Mais </div>
              </div>
            </a>
          </figure>
        </div>
        <div class="produtos-relacionados-2">
          <figure class="figure2">
            <a href="<?=$url?>motoredutor">
              <div class="fig-img2">
                <h2>Motoredutor</h2>
                <div class="btn-5" data-anime="up"> Saiba Mais </div>
              </div>
            </a>
          </figure>
        </div>
        <div class="produtos-relacionados-3">
          <figure>
            <a href="<?=$url?>motor-para-portao-basculante">
              <div class="fig-img">
                <h2>Motor para portão basculante</h2>
                <div class="btn-5" data-anime="up"> Saiba Mais </div>
              </div>
            </a>
          </figure>
        </div>
      </div>
    </section>
    <section class="wrapper-destaque">
      <div class="destaque txtcenter">
        <h2>Galeria de <b>Produtos</b></h2>
        <div class="center-block txtcenter">
          <ul class="gallery">
            <li><a href="<?=$url?>imagens/img-home/galeria/corrente-01.jpg" class="lightbox" title="Motor corrente">
              <img loading="lazy" src="<?=$url?>imagens/img-home/galeria/thumbs/corrente-01.jpg" title="Motor corrente" alt="Motor corrente">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/galeria/eletrico-01.jpg" class="lightbox"  title="Moto elétrico">
              <img loading="lazy" src="<?=$url?>imagens/img-home/galeria/thumbs/eletrico-01.jpg" alt="Moto elétrico" title="Moto elétrico">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/galeria/hidraulico-01.jpg" class="lightbox" title="Motor hidráulico">
              <img loading="lazy" src="<?=$url?>imagens/img-home/galeria/thumbs/hidraulico-01.jpg" alt="Motor hidráulico" title="Motor hidráulico">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/galeria/inducao-01.jpg" class="lightbox" title="Motor por indução">
              <img loading="lazy" src="<?=$url?>imagens/img-home/galeria/thumbs/inducao-01.jpg" alt="Motor por indução" title="Motor por indução">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/galeria/manutencao-01.jpg" class="lightbox" title="Manutenção de motor">
              <img loading="lazy" src="<?=$url?>imagens/img-home/galeria/thumbs/manutencao-01.jpg" alt="Manutenção de motor"  title="Manutenção de motor">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/galeria/motores-01.jpg" class="lightbox" title="Motores">
              <img loading="lazy" src="<?=$url?>imagens/img-home/galeria/thumbs/motores-01.jpg" alt="Motores" title="Motores">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/galeria/motores-02.jpg" class="lightbox" title="Motor">
              <img loading="lazy" src="<?=$url?>imagens/img-home/galeria/thumbs/motores-02.jpg" alt="Motor" title="Motor">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/galeria/pecas-01.jpg" class="lightbox" title="Peças para motor">
              <img loading="lazy" src="<?=$url?>imagens/img-home/galeria/thumbs/pecas-01.jpg" alt="Peças para motor" title="Peças para motor">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/galeria/portao-01.jpg" class="lightbox" title="Motor para portão">
              <img loading="lazy" src="<?=$url?>imagens/img-home/galeria/thumbs/portao-01.jpg" alt="Motor para portão" title="Motor para portão">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/galeria/servicos-01.jpg" class="lightbox" title="Serviços de motor">
              <img loading="lazy" src="<?=$url?>imagens/img-home/galeria/thumbs/servicos-01.jpg" alt="Serviços de motor" title="Serviços de motor">
            </a>
            </li>
          </ul>
        </div>
      </div>
</section>
<? include('inc/form-mpi.php'); ?>
</main>
<? include('inc/footer.php'); ?>
<link rel="stylesheet" href="<?=$url?>nivo/nivo-slider.css" media="screen">
<script  src="<?=$url?>nivo/jquery.nivo.slider.js"></script>
<script >
$(window).load(function() {
$('#slider').nivoSlider();
});
</script>
<script src="<?=$url?>hero/js/modernizr.js"></script>
<script src="<?=$url?>hero/js/main.js"></script>
<script type="text/javascript" src="slick/slick.min.js"></script>
<script>
$('.products').slick({
dots: true,
infinite: true,
speed: 300,
autoplay: true,
slidesToShow: 4,
slidesToScroll: 4,
responsive: [
{
breakpoint: 1024,
settings: {
slidesToShow: 3,
slidesToScroll: 3,
infinite: true,
dots: true
}
},
{
breakpoint: 600,
settings: {
slidesToShow: 2,
slidesToScroll: 2
}
},
{
breakpoint: 480,
settings: {
slidesToShow: 1,
slidesToScroll: 1
}
}
// You can unslick at a given breakpoint now by adding:
// settings: "unslick"
// instead of a settings object
]
});
</script>
</body>
</html>