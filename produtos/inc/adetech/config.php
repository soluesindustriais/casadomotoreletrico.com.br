<?php
$clienteAtivo = "ativo";

//HOMEsolucs
// $tituloCliente recebe ($clienteAtivo igual 'inativo') recebe 'Soluções industriais' se não 'Base Mini Site';
$tituloCliente = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'Adetech';
$tituloCliente = str_replace(' ', '-', $tituloCliente);

$subTituloCliente = 'Produtos Eletromecânicos LTDA';
$bannerIndex = 'imagem-banner-fixo';
$minisite = "inc/" . $tituloCliente . "/";
$subdominio = str_replace(' ', '-', $tituloCliente);




//informacoes-vetPalavras
$CategoriaNameInformacoes = "informacoes";


include("$linkminisite" . "conteudos-minisite.php");


// Criar página de produto mpi
$VetPalavrasProdutos = [
    "escovas-de-carvao-para-aspiradores",
    "escova-para-motor-corrente-continua",
    "aneis-coletores-motores-eletricos",
    "escova-de-carvao-para-motores",
    "cordoalha-chata-de-cobre-estanhado",
    "escova-de-carvao-para-motor-eletrico",
    "isolador-de-pino-polimerico",
    "pantografo-eletrico",
    "escovas-de-carvao-para-motores-eletricos",
    "cordoalha-para-aterramento",
];

//Criar página de Serviço
$VetPalavrasInformacoes = [

];



// Numero do formulario de cotação
$formCotar = 237;



// Informações Geral.php

$nomeSite = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'Adetech';
$slogan = ($clienteAtivo == 'inativo') ? "Inovando sua indústria com eficiência" : 'Produtos Eletromecânicos LTDA';
$rua = ($clienteAtivo == 'inativo') ? "Rua Alexandre Dumas" : 'Rua Estrela Dalva, 194';
$bairro = ($clienteAtivo == 'inativo') ? "Santo Amaro" : 'Cumbica';
$cidade = ($clienteAtivo == 'inativo') ? "São Paulo" : 'Guarulhos';
$UF = ($clienteAtivo == 'inativo') ? "SP" : 'SP';
$cep = ($clienteAtivo == 'inativo') ? "CEP: 04717-004" : '07232-040';
$imgLogo = ($clienteAtivo == 'inativo') ? 'logo-site.webp' : 'logo-cliente-fixo.svg';
$emailContato = ($clienteAtivo == 'inativo') ? '' : 'maze@maze.ind.br';
$instagram =  ($clienteAtivo == 'inativo') ? 'https://www.instagram.com/solucoesindustriaisoficial?igsh=MWtvOGF4aXBqODF3MQ==' : '';
$facebook =  ($clienteAtivo == 'inativo') ? 'https://www.facebook.com/solucoesindustriais/' : '';



?>


<!-- VARIAVEIS DE CORES -->
<style>
    :root {
        --cor-pretopantone: #2d2d2f;
        --cor-principaldocliente: #ff0009;
        --cor-secundariadocliente: #4876b1;
        --terceira-cor: #2d2d2f;
        --font-primary: "Poppins";
        --font-secundary: "Poppins";
    }
</style>