document.addEventListener("DOMContentLoaded", function () {
    const dropdownBtn = document.querySelectorAll(".dropdown-btn-header");
    const dropdown = document.querySelectorAll(".dropdown-header");
    const hamburgerBtn = document.getElementById("hamburger");
    const navMenu = document.querySelector(".menu-header");

    function isMobileDevice() {
        return window.innerWidth <= 1070;  // Ajuste conforme necessário
    }

    function setAriaExpandedFalse() {
        dropdownBtn.forEach(btn => btn.setAttribute("aria-expanded", "false"));
    }

    function closeDropdownMenu() {
        dropdown.forEach(drop => {
            drop.classList.remove("active");
            drop.addEventListener("click", (e) => e.stopPropagation());
        });
    }

    function setupDesktopDropdown() {
        dropdownBtn.forEach(btn => {
            if (btn.dataset.originalHref) {
                btn.href = btn.dataset.originalHref;
            }

            btn.addEventListener("mouseenter", function () {
                const dropdownIndex = this.getAttribute("data-dropdown");
                const dropdownElement = document.getElementById(dropdownIndex);
                dropdownElement.classList.add("active");
                this.setAttribute("aria-expanded", "true");
                this.querySelector(".fa-chevron-right").classList.add("rotate");
            });

            btn.parentNode.addEventListener("mouseenter", function() {
                this.querySelector('.dropdown-header').classList.add("active");
            });

            btn.parentNode.addEventListener("mouseleave", function () {
                const dropdownIndex = btn.getAttribute("data-dropdown");
                const dropdownElement = document.getElementById(dropdownIndex);
                dropdownElement.classList.remove("active");
                btn.setAttribute("aria-expanded", "false");
                btn.querySelector(".fa-chevron-right").classList.remove("rotate");
            });
        });
    }

    function setupMobileDropdown() {
        dropdownBtn.forEach(btn => {
            btn.removeAttribute("href");  // Remover href nos dispositivos móveis

            btn.addEventListener("click", function (e) {
                e.preventDefault(); // Impedir comportamento padrão do link
                e.stopPropagation(); // Impede que o evento de clique propague
                const dropdownIndex = this.getAttribute("data-dropdown");
                const dropdownElement = document.getElementById(dropdownIndex);
    
                const isActive = dropdownElement.classList.contains("active");
                setAriaExpandedFalse(); // Fecha todos os dropdowns
                closeDropdownMenu();
    
                if (!isActive) {
                    dropdownElement.classList.add("active");
                    this.setAttribute("aria-expanded", "true");
                } else {
                    this.setAttribute("aria-expanded", "false");
                }
            });
        });

        document.addEventListener("click", () => {
            closeDropdownMenu();
            setAriaExpandedFalse();
        });
    }

    function toggleHamburger() {
        navMenu.classList.toggle("show");
        const expanded = hamburgerBtn.getAttribute("aria-expanded") === "true";
        hamburgerBtn.setAttribute("aria-expanded", !expanded);
    }

    hamburgerBtn.addEventListener("click", toggleHamburger);

    // Aplicar lógica baseada no tipo de dispositivo
    if (isMobileDevice()) {
        setupMobileDropdown();
    } else {
        setupDesktopDropdown();
    }

    window.addEventListener('resize', function () {
        // Redefine os listeners quando o tamanho da janela muda
        if (isMobileDevice()) {
            setupMobileDropdown();
        } else {
            setupDesktopDropdown();
        }
    });
});
