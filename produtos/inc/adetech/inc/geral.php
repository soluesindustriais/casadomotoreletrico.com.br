<?

$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") {
  $url = $http . "://" . $host . "/";
} else {
  $url = $http . "://" . $host . $dir["dirname"] . "/";
}

$nomeSateliteP = "casadomotoreletrico";

$urlSatelite = "https://www.$nomeSateliteP.com.br/";

// include 'config.php';

$latitude = '-22.546052';
$longitude = '-48.635514';
$segmento = 'Comercio';

$mapa = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.887269188617!2d-46.69574088497707!3d-23.608375569215365!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce50cb1288720d%3A0x6806a285858e9a97!2sAv.%20Engenheiro%20Lu%C3%ADs%20Carlos%20Berrini%2C%201297%20-%20Itaim%20Bibi%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2004571-010!5e0!3m2!1spt-BR!2sbr!4v1661112493683!5m2!1spt-BR!2sbr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';
// MAP CLEANUP
preg_match('/(?<=src=").*?(?=[\"])/', $mapa, $out);
$mapa = $out[0];


$ddd = '12';
$fone[0] = array($ddd, '97416-0043', '<i class="fa-solid fa-phone"></i>');
$fone[1] = array($ddd, '3513-8936', 'fab fa-whatsapp');



$senhaEmailEnvia = '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode = explode("/", $_SERVER['REQUEST_URI']);
$urlPagina = end($explode);
$urlPagina = str_replace('.php', '', $urlPagina);
$urlPagina == "index" ? $urlPagina = "" : "";
$urlAnalytics = str_replace("https://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
//reCaptcha do Google
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';
//********************COM SIG APAGAR********************
//Gerar htaccess automático
$urlhtaccess = $url;
$schemaReplace = 'https://www.';
$urlhtaccess = str_replace($schemaReplace, '', $urlhtaccess);
$urlhtaccess = rtrim($urlhtaccess, '/');
define('RAIZ', $url);
define('HTACCESS', $urlhtaccess);
include('inc/gerador-htaccess.php');
//********************FIM SIG APAGAR********************

// MENU

// Menu items (link, text, hasSubmenu, NULL [FontAwesome - ex: building])

$sigMenuPosition = false; //INSIRA A POSIÇÃO DO MENU DO SIG EM RELAÇÃO AOS DEMAIS ITENS (EX.: $sigMenuPosition = 3;)

// CASO MENU BRASMODULOS
// INSIRA OS ÍCONES NA ORDEM EM QUE OS ITENS DO SIG SÃO ADICIONADOS (EX.: ['home', 'cog', 'info'];)
// Exemplo: $sigMenuIcons = ['home', 'cog', 'wrench'];
$sigMenuIcons = [];
// FIM CASO MENU BRASMODULOS

$menu[0] = array('', 'Inicio', false, NULL);
$menu[1] = array('sobre-nos', 'Sobre nós', false, NULL);
$menu[2] = array('sobre-nos', 'Contato', false, NULL);
$menu[3] = array('produtos', 'Informações', 'sub-menu', 'fa-solid fa-chevron-down');

// END MENU

// Variáveis de teste para envio de e-mail
// --------------- IMPORTANTE!!!! --------------- 
// Altere para true quando fizer o teste e volte para false após finalizá-lo
$envioTeste = false;
// Insira seu email para fazer o teste de envio
$emailTeste = 'SEU_EMAIL_AQUI@DOUTORESDAWEB.COM.BR';

//Pasta de imagens, Galeria, url Facebook, etc.
$pasta = 'imagens/informacoes/';
$author = ''; // Link do perfil da empresa no g+ ou deixar em branco


$parts = explode('.', $host);
$desiredPart = $parts[count($parts) - 3];
$linkdominio = "$http"."://www.$desiredPart.com.br/produtos/";
$hostName = parse_url($url, PHP_URL_HOST);

if ($hostName === 'localhost') {
    $linkdominio = $url; 
}

// Verifica se a URL contém 'minisite-base'
if (strpos($url, 'minisite-base') !== false) {
    $minisite = "";
    $linkminisite = "";

} 

$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
$hostnw = preg_replace('/^www\./', '', $host);
$linksubdominio = $http . '://'. $subdominio.'.'. $hostnw .'/';


//Redes sociais
// $paginaFacebook	 = 'PAGINA_DO_FACEBOOK_DO_CLIENTE';
// $urlInstagram		= 'URL_COMPLETA_INTAGRAM';
// $urlYouTube			= 'URL_COMPLETA_YOUTUBE';
// $urlLinkedIn			= 'URL_COMPLETA_LINKEDIN';
// $urlTwitter			= 'URL_COMPLETA_TWITTER';


// include('inc/classes/trataString.class.php');
// include('inc/vetCategorias.php');
// include('inc/classes/criarCategoria.class.php');
// $trata = new Trata();
// $categorias = new Categoria();
// $categorias->setCategorias($vetCategorias);
// $categorias->criarCategoria();

// //Função para gerar os arquivos as palvras na raiz
// //Comentar a mesma ao fazer o deploy

// include('inc/informacoes/informacoes-vetPalavras.php');
// include('inc/classes/criarPalavras.class.php');
// $trata = new Trata();
// $palavras = new Palavra();
// $palavras->setPalavras($VetPalavrasInformacoes);
// $palavras->criarPalavra();


// include('inc/classes/criarPalavrasProdutos.class.php');
// $trata = new Trata();
// $produtos = new Produto();
// $produtos->setprodutos($VetPalavrasProdutos);
// $produtos->criarProduto();

//Função para gerar os arquivos *-categoria.php de cada categoria na raiz e gerar os folders de cada categoria na inc/
//Comentar a mesma ao fazer o deploy


//Breadcrumbs
$autoBreadcrumb = '
  <div class="background-bread bread bread--default bread--auto">
    <div class="breadcrumb">
      <div class="wrapper bread-content">
        <div class="bread__row">
          <h2 class="bread__title">' . $nomeSite . ' </h2>
          <h1 class="bread__title">' . $h1 . '</h1>
  
          <nav class="bread-list" aria-label="breadcrumb">
            <ol id="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
              <div class="item-breadcrumb">
                <li class="bread__column breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a href="' . $url . '" itemprop="item" title="Início">
                            <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Início ❱ </span>
                            </a>
                            <meta itemprop="position" content="1">
                </li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>';
$caminho = '
<div class="background-bread-produtos">
  <div class="breadcrumb"">
      <div class="wrapper bread-content">
          <div class="bread__row">
          <h2 class="bread__title">' . $nomeSite . ' </h2>
          <h1 class="bread__title">' . $h1 . '</h1>
  <nav aria-label="breadcrumb" class="bread-list">
    <ol id="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
      <div class="item-breadcrumb">
        <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
          <a href="' . $urlSatelite . '" itemprop="item" title="Home">
            <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Início ❱ </span>
          </a>
          <meta itemprop="position" content="1" />
        </li>
        <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
          <span itemprop="name">' . $h1 . '</span>
          <meta itemprop="position" content="2" />
        </li>
      </div>
    </ol>
  </nav>
  </div>
  </div>
  </div>
</div>
  ';
$caminhocateg = '
<div class="background-bread-categ">
  <div class="breadcrumb" id="breadcrumb  ">
      <div class="wrapper-produtos">
          <div class="bread__row">
          <h1 class="bread__title">' . $h1 . '</h1>
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
      <div class="item-breadcrumb">
        <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
          <a href="' . $urlSatelite . '" itemprop="item" title="Home">
            <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Início ❱ </span>
          </a>
          <meta itemprop="position" content="1" />
        </li>
        <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a  href="' . $urlSatelite . '" itemprop="item" title="Produtos">
    <span itemprop="name">Produtos ❱</span>
        </a>
        <meta itemprop="position" content="2" />
        </li>
        <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
          <span itemprop="name">' . $h1 . '</span>
          <meta itemprop="position" content="3" />
        </li>
      </div>
    </ol>
  </nav>
  </div>
  </div>
  </div>
</div>
  ';
$caminhoartigos = '
        <div class="background-bread artigo">
          <div class="breadcrumb" id="breadcrumb  ">
              <div class="wrapper-produtos">
                  <div class="bread__row">
                  <h1 class="bread__title">' . $h1 . '</h1>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
              <div class="item-breadcrumb">
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                  <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Início ❱ </span>
                  </a>
                  <meta itemprop="position" content="1" />
                </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="' . $url . 'artigos" itemprop="item" title="Artigos">
                <span itemprop="name">Artigos ❱</span>
                    </a>
                    <meta itemprop="position" content="2" />
                    </li>
                  
                <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                  <span itemprop="name">' . $h1 . '</span>
                  <meta itemprop="position" content="3" />
                </li>
              </div>
            </ol>
          </nav>
          </div>
          </div>
          </div>
        </div>
          ';
          $caminhoservicos = '
          <div class="background-bread-produtos">
          <div class="breadcrumb">
              <div class="wrapper bread-content">
                  <div class="bread__row">
                  <h2 class="bread__title">' . $nomeSite . ' </h2>
                  <h1 class="bread__title">' . $h1 . '</h1>
          <nav aria-label="breadcrumb" class="bread-list">
            <ol id="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
              <div class="item-breadcrumb">
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                  <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Início ❱ </span>
                  </a>
                  <meta itemprop="position" content="1" />
                </li>
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                  <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"> Informacoes ❱ </span>
                  </a>
                  <meta itemprop="position" content="1" />
                </li>
                <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                  <span itemprop="name">' . $h1 . '</span>
                  <meta itemprop="position" content="2" />
                </li>
              </div>
            </ol>
          </nav>
          </div>
          </div>
          </div>
        </div>
          ';
        

$isMobile = preg_match("/(android|webos|avantgo|iphone|ipad|ipod|blackberry|iemobile|bolt|boost|cricket|docomo|fone|hiptop|mini|opera mini|kitkat|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
