
<?php

/*====================
|___Marcelo Serra____|
|  Data: 28/08/2020  |
====================*/


/*====================
|_____duda godoi_____|
|  Data: 03/01/2023  |
====================*/



class Palavra
{
    private $palavras;

    public function setpalavras($array)
    {
        $this->palavras = $array;
    }

    public function getpalavras()
    {
        return $this->palavras;
    }

    private function criarPaginaRaiz($file, $string)
    {
        $trata = new Trata();
        $stringUpperSemHifen = ucwords($trata->retiraHifen($string));
        
        $text =
            '
            <?php
            $h1             = "' . $stringUpperSemHifen . '";
            $title          = "' . $stringUpperSemHifen . '";
            $desc           = "' . $stringUpperSemHifen . ' - Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt eos eaque aliquid nisi ipsa modi, sed reprehenderit dicta harum est ratione nulla, doloremque dolor atque maxime aliquam, tenetur nam deleniti!";
include $minisite . "inc/head.php";
include $minisite . "inc/fancy.php";
?>
<style>
  <? include $minisite . "css/mpi.css"; ?>
</style>
<script src="<?= $url . $minisite ?>js/organictabs.jquery.js" defer></script>
<link rel="stylesheet" href="inc/<?=$minisite?>/css/aside.css">
</head>

<body>
  <? include $minisite . "inc/topo.php"; ?>
  <main class="main-tag-content">
    <div class="content" itemscope itemtype="https://schema.org/Article">
      <section>
        <?= $caminho ?>
        <div class="wrapper main-mpi-container">
          <article>
            <div class="article-content">
              <div class="ReadMore" style="overflow: hidden; height: auto; transition: height 100ms ease-in-out 0s;">
                <h2 class="h2-description">Descrição</h2>
                <p class="p-description">

                  <?= $conteudoPagina?>

                </p>
                <span id="readmore-open">Continuar Lendo <i class="fa-solid fa-turn-down" style="color: var(--cor-pretopantone);"></i></span>
                <span id="readmore-close">Fechar <i class="fa-solid fa-turn-up" style="color: var(--cor-pretopantone);"></i></span>
              </div>
            </div>
            <?
            include $minisite . "inc/gallery.php";
            ?>
            <?
            include $minisite . "inc/card-informativo.php";
            // include("inc/especificacoes-mpi.php");
            ?>
          </article>
          <?
          include $minisite . "inc/coluna-lateral.php";
          include $minisite . "inc/regioes.php";
          include $minisite . "inc/aside-produtos.php";

          include $minisite . "inc/copyright.php";
          ?>
        </div><!-- .wrapper -->
        <div class="clear"></div>
      </section>
    </div>
  </main>
  <? include $minisite . "inc/footer.php"; ?>

</body>

<script>
  document.addEventListener("DOMContentLoaded", function() {
    // Seleciona todos os elementos com a classe "p-side"
    const pSideElements = document.querySelectorAll(".p-side");

    pSideElements.forEach(element => {
      // Adiciona um ouvinte de evento de clique a cada elemento "p-side"
      element.addEventListener("click", function() {
        // Primeiro, remove a classe "p-side-active" de todos os elementos "p-side"
        pSideElements.forEach(el => {
          el.classList.remove("p-side-active");
        });

        // Adiciona a classe "p-side-active" ao elemento clicado
        element.classList.add("p-side-active");
      });
    });
  });
</script>

</html>
            ';

        $this->writeText($file, $text);
    }

    private function writeText($file, $string)
    {
        $handle = fopen($file, "w");
        fwrite($handle, $string);
        fclose($handle);
    }


    public function criarPalavra()
    {
        foreach ($this->palavras as $palavra) {
            $this->checkFileExists($palavra);
        }
    }

    private function checkFileExists($string)
    {
        $trata = new Trata();
        $stringSemAcentos = $trata->trataAcentos($string);

        $stringSemAcentos = $trata->retiraHifen($stringSemAcentos);
        $stringSemAcentos = $trata->insereHifen($stringSemAcentos);
        
        $file = $stringSemAcentos . ".php";
        if (!file_exists("./" . $file)) {
            $this->criarPaginaRaiz($file, $string);
        }
    }

    private function checkDirExists($string)
    {
        $trata = new Trata();
        $stringSemAcentos = $trata->trataAcentos($string);
        $file = $stringSemAcentos;
        if (!is_dir("./inc/" . $file)) {
            mkdir("./inc/" . $file, 0777);
        }
    }
};

?>

