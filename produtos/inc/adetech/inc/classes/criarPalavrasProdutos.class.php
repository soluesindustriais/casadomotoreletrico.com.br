
<?php

class Produto
{
  private $produtos;

  public function setprodutos($array)
  {
    $this->produtos = $array;
  }

  public function getprodutos()
  {
    return $this->produtos;
  }

  private function criarPaginaRaiz($file, $string)
  {
    $trata = new Trata();
    $stringUpperSemHifen = ucwords($trata->retiraHifen($string));

    $text =
      '
      <?php
$h1             = "' . $stringUpperSemHifen . '";
$title          = "' . $stringUpperSemHifen . '";
$desc           = "' . $stringUpperSemHifen . ' - Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt eos eaque aliquid nisi ipsa modi, sed reprehenderit dicta harum est ratione nulla, doloremque dolor atque maxime aliquam, tenetur nam deleniti!";
$key            = "mpi,sample,lorem,ipsum";
$legendaImagem  = "Foto ilustrativa de Armário de aço tipo roupeiro preço";
$pagInterna     = "Informações";
$urlPagInterna  = "informacoes";
$nomeurl = pathinfo(basename($_SERVER["PHP_SELF"]), PATHINFO_FILENAME);
include $minisite . "inc/head-mpi.php";
include $minisite . "inc/fancy.php";
?>
<link rel="stylesheet" href="<?=$minisite?>css/mpi-product.css">
<link rel="stylesheet" href="<?=$minisite?>css/mpi.css">
<link rel="stylesheet" href="<?=$minisite?>css/aside.css">
<style>
  body {
    scroll-behavior: smooth;
  }
</style>
<script src="<?= $url . $minisite ?>js/organictabs.jquery.js" defer></script>

</head>

<body>
  <? include $minisite . "inc/topo.php"; ?>
  <main>
    <div class="content" itemscope itemtype="https://schema.org/Article">
      <?= $caminhocateg ?>
      <div class="main-tag-content">
        <div class="container-main-produtos">
          <section class="anuncio-principal wrapper flex-wrap-mobile light-box-shadow">
            <section class="content-anuncio-principal">
              <div class="imagens-anuncio">
                <?php include("inc/gallery-product.php") ?>
              </div>
              <div class="description">
                <hr class="mobile-line">
                <div class="article-content">
                  <div class="ReadMore" style="overflow: hidden; height: auto; transition: height 100ms ease-in-out 0s;">
                    <h2 class="h2-description">Descrição</h2>
                    <p class="p-description">
                      <?= $conteudoPagina[0] // trocar o valor do array pelo valor do conteudo que corresponde a página?>
                    </p>
                    <span id="readmore-open">Continuar Lendo <i class="fa-solid fa-turn-down" style="color: var(--cor-pretopantone);"></i></span>
                    <span id="readmore-close">Fechar <i class="fa-solid fa-turn-up" style="color: var(--cor-pretopantone);"></i></span>
                  </div>
                </div>
              </div>
            </section>
            <aside class="especificacoes-anuncio-principal">
              <h1 class="h1-title-anuncio"><?= $h1?></h1>
              <p class="p-nivel-de-procura">
                Nivel de Procura:
                <span id="span-nivel-de-procura">alta</span>
              </p>
              <div class="caracteristicas-produto">
                <div class="litros-aside l-aside">
                  <p class="ptitle-side">Litros</p>
                  <div class="op-aside">
                    <p class="p-side p-side-active">100 L</p>
                    <p class="p-side">200 L</p>
                    <p class="p-side ">300 L</p>
                  </div>
                </div>
                <div class=" l-aside">
                  <p class="ptitle-side">Comprimento</p>
                  <div class="option-comprimento op-aside">
                    <p class="p-side">10 M</p>
                    <p class="p-side p-side-active">20 M</p>
                    <p class="p-side">30 M</p>
                  </div>
                </div>
              </div>
              <div class="espec-tecnicas">
                <h2 class="h2-espec-tecnicas">Especificações Técnicas</h2>
                <ul class="ul-espec-tecnicas">
                  <li>Exemplo Especificação</li>
                  <li>Exemplo Especificação</li>
                </ul>
              </div>
              <button class="botao-cotar botao-cotar-mobile btn-solicitar-orcamento" title="<?= $h1 ?>">Solicite um Orçamento</button>
              <? include $minisite . "inc/btn-cotar.php"; ?>
              <div class="icons-anuncio">
                <div class="icon-regiao icon-aside">
                  <img src="<?=$minisite?>/imagens/icons/regiao-atendimento.png" alt=" ICONE REGIÃO DE ATENDIMENTO" />
                  <div class="text-icon">
                    <h2>Região de Atendimento</h2>
                    <p><?= $cidade?></p>
                  </div>
                </div>
                <div class="icon-categoria icon-aside">
                  <img src="<?=$minisite?>/imagens/icons/icon-categoria.png" alt=" ICONE REGIÃO DE ATENDIMENTO" />
                  <div class="text-icon">
                    <h2>Categoria</h2>
                    <p>Produtos</p>
                  </div>
                </div>
                <div class="icon-sub-categoria icon-aside">
                  <img src="<?=$minisite?>/imagens/icons/sub-categoria.png" alt=" ICONE REGIÃO DE ATENDIMENTO" />
                  <div class="text-icon">
                    <h2>Sub Categoria</h2>
                    <p><?= $nomeurl?></p>
                  </div>
                </div>
              </div>
            </aside>
          </section>
          <div class="wrapper content-produtos">
            <? include $minisite . "inc/produtos-populares.php"; ?>
            <? include $minisite . "inc/produtos-relacionados.php"; ?>
            <? include $minisite . "inc/aside-produtos.php"; ?>
            <? include $minisite . "inc/regioes.php"; ?>
            <div class="clear"></div>
          </div>
        </div>
      </div>
    </div>
  </main>
  <script src="<?= $minisite?>js/img-alternate-product.js"></script>
  <? include $minisite . "inc/footer.php"; ?>
</body>
<script>
  document.addEventListener("DOMContentLoaded", function() {
    // Seleciona todos os elementos com a classe "p-side"
    const pSideElements = document.querySelectorAll(".p-side");

    pSideElements.forEach(element => {
      // Adiciona um ouvinte de evento de clique a cada elemento "p-side"
      element.addEventListener("click", function() {
        // Primeiro, remove a classe "p-side-active" de todos os elementos "p-side"
        pSideElements.forEach(el => {
          el.classList.remove("p-side-active");
        });

        // Adiciona a classe "p-side-active" ao elemento clicado
        element.classList.add("p-side-active");
      });
    });
  });
</script>

</html>   
            ';

    $this->writeText($file, $text);
  }

  private function writeText($file, $string)
  {
    $handle = fopen($file, "w");
    fwrite($handle, $string);
    fclose($handle);
  }


  public function criarProduto()
  {
    foreach ($this->produtos as $produto) {
      $this->checkFileExists($produto);
    }
  }

  private function checkFileExists($string)
  {
    $trata = new Trata();
    $stringSemAcentos = $trata->trataAcentos($string);

    $stringSemAcentos = $trata->retiraHifen($stringSemAcentos);
    $stringSemAcentos = $trata->insereHifen($stringSemAcentos);

    $file = $stringSemAcentos . ".php";
    if (!file_exists("./" . $file)) {
      $this->criarPaginaRaiz($file, $string);
    }
  }

  private function checkDirExists($string)
  {
    $trata = new Trata();
    $stringSemAcentos = $trata->trataAcentos($string);
    $file = $stringSemAcentos;
    if (!is_dir("./inc/" . $file)) {
      mkdir("./inc/" . $file, 0777);
    }
  }
};

?>

