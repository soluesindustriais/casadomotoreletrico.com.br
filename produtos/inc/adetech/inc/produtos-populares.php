<section class="anuncio-secundario light-box-shadow">
<? $linkminisiteprodpop = ($linkminisitenb == "") ? "." : "$linkminisitenb" ?>
    <h2>Produtos <span class="color-secundary-mpi">Populares</span></h2>

    <section class="produtos-vistos-container flex-wrap-mobile">
        <?php
        $limit = 4; // Número de divs a serem exibidas
        $codigo = 1;
        $codImg = 1;

        for ($i = 0; $i < $limit; $i++) {
            // Supondo que você quer repetir o mesmo conteúdo, ajuste conforme necessário
            $pagina = $VetPalavrasProdutos[$i]; // Apenas como exemplo, usando o primeiro item
            $palavraSemAcento = strtolower(remove_acentos($pagina));
            $palavraComHifen = ucwords(str_replace(" ", "-", $pagina));
            $palavraSemHifenUpperCase = ucwords(str_replace("-", " ", $pagina));
            
            echo "
            <div class='card-produtos-vistos light-box-shadow'>
            <div class='cod-produto'><p>Cod:<span> <strong>#00$codigo</strong></span></p><p>Em alta <i class='fa-solid fa-fire' style='color:#FFAC1E'></i></p></div>
            <img src='$linkminisiteprodpop/imagens/informacoes/$palavraSemAcento-$codImg.webp' alt='$palavraSemHifenUpperCase'>
            <p class='p-pagina-relacionada'>$palavraSemHifenUpperCase</p>
            <a class='btn-produtos-vistos' href=\"" . $url  . $palavraSemAcento . "\">Mais informações</a>
            </div>
            ";
            $codigo++;
        }
        ?>


        
    </section>
</section>