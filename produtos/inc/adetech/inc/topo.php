<header id="scrollheader">

    <div class="wrapper">
        <div class="d-flex align-items-center justify-content-between">
            <div class="logo"><a rel="nofollow" href="<?= $url ?>" title="Voltar a página inicial"><img src="<?= $linkminisite; ?>imagens/logo-cliente-fixo.svg" alt="<?= $slogan . " - " . $nomeSite ?>" title="<?= $slogan . " - " . $nomeSite ?>"></a></div>
            <div class="d-flex align-items-center justify-content-end">
                <nav id="menu2">
                    <ul>
                        <? include "$linkminisite" . "inc/menu-top.php"; ?>    
                    </ul>
                </nav>
            </div>
            <? include "$linkminisite" . "inc/pesquisa-inc.php"; ?>
        </div>
    </div>

    <div class="clear"></div>
</header>
<div id="header-block"></div>

<style>
    :root {
        --padding-small: 10px 300px;
        --padding-medium: 10px 200px;
        --padding-low: 10px 100px;
        --padding-superlow: 10px 60px;
        --padding-mobile: 10px 30px;
        --padding-extrememobile: 5px 15px;
    }

    .header-scroll {
        display: flex;
        padding: var(--padding-small);
    }

    #nav-scroll-header {
        display: flex;
        align-items: center;
        justify-content: space-between;
        width: 100%;
    }

    @media screen and (min-width: 1400px) and (max-width: 1600px) {
        .header-scroll {
            padding: var(--padding-medium);
        }

    }

    @media screen and (min-width: 1200px) and (max-width: 1399px) {
        .header-scroll {
            padding: var(--padding-low);
        }

    }

    @media screen and (min-width: 1000px) and (max-width: 1199px) {
        .header-scroll {
            padding: var(--padding-superlow);
            width: 100%;
        }

    }

    @media screen and (min-width: 800px) and (max-width: 999px) {
        .header-scroll {
            padding: var(--padding-mobile);

        }


    }

    @media screen and (min-width: 600px) and (max-width:799px) {
        .header-scroll {
            padding: var(--padding-mobile);
        }
    }

    @media screen and (min-width: 320px) and (max-width:599px) {
        .header-scroll {
            padding: 0px 5px;
        }
    }
</style>