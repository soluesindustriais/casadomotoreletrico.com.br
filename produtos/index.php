<?
$minisite = "adetech";
$subdominio = "adetech";
$linkminisite = "inc/$minisite/";


$linkminisitenb = substr($linkminisite, 0, -1);
$clienteAtivo = "ativo";

// Obtém o nome do arquivo atual sem a extensão
$nomeArquivo = pathinfo(basename(__FILE__), PATHINFO_FILENAME);

// Remove os hífens e transforma a primeira letra de cada palavra em maiúscula
$nomeVariavel = ucwords(str_replace('-', ' ', $nomeArquivo));

$h1 = 'Categorias';
$title = 'Categorias';
$desc = "Orce $h1, conheça os    melhores fornecedores, compare hoje com aproximadamente 200 fabricantes ao mesmo tempo";
$key  = "";
include("$linkminisite" . "inc/head.php");
include("$linkminisite" . "inc/informacoes/informacoes-vetPalavras.php"); ?>

<style>
    body {
        scroll-behavior: smooth;
    }

    <?
    include("$linkminisite" . "css/header-script.css");
    include("$linkminisite" . "css/style.css");
    ?>
</style>
</head>

<? include("$linkminisite" . "inc/header-dinamic.php"); ?><main role="main">
    <section> <?= $caminho ?> <div class="wrapper-produtos"> <br class="clear">
            <h1 style="text-align: center;  "><?= $h1 ?></h1>
            <article class="full">
                
                <ul class="thumbnails-main"> <?php include_once("$linkminisite" . "inc/informacoes/informacoes-categoria-index.php"); ?> </ul>
            </article>
            
    </section>
</main>
</div>
<!-- .wrapper --> <? include "$linkminisite" . "inc/footer.php"; ?> </body>

</html>