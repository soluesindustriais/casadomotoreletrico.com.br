<?php
$h1     = 'Contato';
$title  = 'Contato';
$desc   = 'Entre em contato e envie sua mensagem pelo formulário e logo entraremos em contato. Qualquer dúvida estamos a disposição pelo email ou telefone';
$key    = '';
$var    = 'Contato';
include('inc/head.php');
?>
<!-- Alerta -->
<!-- <link href="<?= $url; ?>js/sweetalert/css/sweetalert.css" rel="stylesheet">
<script src="<?= $url; ?>js/sweetalert/js/sweetalert.min.js"></script> -->
<!-- Alerta -->
<!--/Máscara dos campos-->
</head>
<body>
<?php include('inc/topo.php'); ?>
<main class="wrapper">
    <div class="content-contato">
        <section>
            <?= $caminho2 ?>
            <br class="clear"> <br class="clear">
            <h1>Entre em contato e Cote com mais de 60 fornecedores de Motores elétricos. É grátis</h1>
            <div class="container-contato contact">
               <?php
                $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
                if (isset($post) && $post['enviar']):
                unset($post['enviar']);
                include("contato-envia.php");
                endif;
                ?>
                <div class="form col-5 col-m-12">
                    <form method="post" name="form" enctype="multipart/form-data">
                        <label>Nome: <span>*</span> </label>
                        <input name="nome" required="">
                        <label>E-mail: <span>*</span> </label>
                        <input type="email" name="email" required="">
                        <label>Telefone: <span>*</span> </label>
                        <input name="telefone" required="">
                        <label>Produto <span>*</span> </label>
                        <input type="text" name="produto" id="tel" required/>
                        <label>Empresa:
                            <span>*
                            </span>
                        </label>
                        <input type="text" name="empresa" value="" id="empresa" required="">
                        <label>Mensagem <span>*</span> </label>
                        <textarea name="mensagem" rows="5" required=""></textarea>
                        <span class="obrigatorio">Os campos com * são obrigatórios</span>
                        <input type="submit" name="enviar" value="Enviar" class="ir">
                    </form>
                </div>
                <br>
                <div class="col-5" id="fundo-contato"></div>
            </div>
        </section>
    </div>
</main>
<?php include('inc/footer.php');?>
</body>
</html>